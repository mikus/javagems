package mo.javagems;

import java.util.*;

abstract class Box<T> {
    private T value;

    public Box(T initialValue) {
        this.value = initialValue;
    }

    public T get() {
        return this.value;
    }

    @SuppressWarnings("unchecked")
    public <Z> Z cast() {
        return (Z)this.value;
    }

    public void set(T value) {
        this.value = value;
    }

    public abstract void set(String textValue);
}

class StringBox extends Box<String> {
    public StringBox() {
        super("wartosc poczatkowa");
    }

    public void set(String textValue) {
        super.set(textValue);
    }
}

class IntegerBox extends Box<Integer> {
    public IntegerBox() {
        super(-1);
    }

    public void set(String textValue) {
        set(Integer.parseInt(textValue));
    }
}

public class BoxDemo {

    public static void main(String[] args) {
        StringBox stringBox1 = new StringBox();
        StringBox stringBox2 = new StringBox();
        stringBox2.set("takie tam...");

        IntegerBox integerBox1 = new IntegerBox();
        IntegerBox integerBox2 = new IntegerBox();
        integerBox2.set(22);
        IntegerBox integerBox3 = new IntegerBox();
        integerBox3.set("333");

        List<? extends Box<?>> boxes = Arrays.asList(stringBox1, stringBox2, integerBox1, integerBox2, integerBox3);

        for (Box<?> box : boxes) {
            Object value = box.get();
            System.out.println("value " + value + " of " + value.getClass());
        }

        String testString = boxes.get(1).cast();
        int testInteger = integerBox1.get();
    }

}
