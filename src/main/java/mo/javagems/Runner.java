package mo.javagems;

import java.util.*;
import java.lang.reflect.*;
import java.io.IOException;
import com.google.common.reflect.*;
import com.google.common.collect.*;
import com.google.common.base.*;

public class Runner {

    public static void main(String args[]) {
        if (args.length > 0)
            automaticMode(args);
        else
            interactiveMode();
    }

    private static void automaticMode(String args[]) {
        for (String className : args) {
            Class<?> clazz = findClass(className);
            if (clazz != null)
                runMainOf(clazz);
        }
    }

    private static Class<?> findClass(String className) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException _) {
            try {
                clazz = Class.forName(Runner.class.getPackage().getName() + "." + className);
            } catch (ClassNotFoundException __) { }
        }
        return clazz;
    }

    private static void runMainOf(Class<?> clazz) {
        try {
            Method mainMethod = clazz.getMethod("main", String[].class);
            System.out.println("\nRunning " + clazz.getName());
            mainMethod.invoke(null, new Object[] { new String[0] });
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException _) { }
    }

    private static void interactiveMode() {
        List<ClassPath.ClassInfo> classInfos = new ArrayList<>(findAllClasses());
        Scanner in = new Scanner(System.in);

        int selection = -1;
        while (selection != 0) {
            displayMenu(classInfos);
            selection = in.nextInt();
            if (selection > 0 && selection <= classInfos.size())
                runMainOf(classInfos.get(selection - 1).load());
        }
        in.close();
    }

    private static Collection<ClassPath.ClassInfo> findAllClasses() {
        try {
            ClassPath cp = ClassPath.from(Runner.class.getClassLoader());
            ImmutableSet<ClassPath.ClassInfo> allClasses = cp.getTopLevelClasses(Runner.class.getPackage().getName());
            return Collections2.filter(allClasses, new Predicate<ClassPath.ClassInfo>() {
                public boolean apply(ClassPath.ClassInfo input) {
                    if (!input.getName().equals(Runner.class.getName())) {
                        Class<?> clazz = input.load();
                        try {
                            Method mainMethod = clazz.getMethod("main", String[].class);
                            return true;
                        } catch (NoSuchMethodException __) { }
                    }
                    return false;
                }
            });

        } catch (IOException _) {
            return Collections.emptyList();
        }
    }

    private static void displayMenu(List<ClassPath.ClassInfo> classInfos) {
        System.out.println("\nJavGems menu");
        for (int i = 0; i < classInfos.size(); ++i) {
            System.out.println((i+1) + ": " + classInfos.get(i).getSimpleName());
        }
        System.out.println("0: EXIT");
        System.out.print("\nYour choice: ");
    }

}
